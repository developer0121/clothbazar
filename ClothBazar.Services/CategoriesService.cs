﻿using ClothBazar.Entities;
using ClotheBazar.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Services
{
    public class CategoriesService
    {

        public List<Category> GetCategories()
        {
            using (CBContext db = new CBContext())
            {
                return db.Categories.ToList();
            }
        }

        public Category GetCategory(int ID)
        {
            using (CBContext db = new CBContext())
            {
                return db.Categories.Find(ID);
            }
        }
        

       public List<Category> GetFeaturedCategories()
        {
            using (CBContext db = new CBContext())
            {
                return db.Categories.Where(x => x.IsFeatured==true && x.ImageURL !=null).ToList();
            }
        }


        public void SaveCategory(Category category)
        {
            using (CBContext db = new CBContext())
            {
                db.Categories.Add(category);
                db.SaveChanges();
            }
        }

        public void UpdateCategory(Category category)
        {
            using (CBContext db = new CBContext())
            {
                db.Entry(category).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }


        public void DeleteCategory(int ID)
        {
            using (CBContext db = new CBContext())
            {
                var category = db.Categories.Find(ID);
                db.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }
        }



    }
}
