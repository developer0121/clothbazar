﻿using ClothBazar.Entities;
using ClotheBazar.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClothBazar.Services
{
    public class ProductsService
    {
        
        public List<Product> GetProducts()
        {
            using (CBContext db = new CBContext())
            {
                return db.Products.Include(x => x.category).ToList();
            }
        }

        public List<Product> GetProducts(List<int> IDs)
        {
            using (CBContext db = new CBContext())
            {
                return db.Products.Where(x => IDs.Contains(x.ID)).ToList();
            }
        }

        public Product GetProduct(int id)
        {
            using (CBContext db = new CBContext())
            {
                return db.Products.Find(id);
            }
        }


        public void SaveProduct(Product product)
        {
            using (CBContext db = new CBContext())
            {
                db.Entry(product.category).State = System.Data.Entity.EntityState.Unchanged;
                db.Products.Add(product);
                db.SaveChanges();
            }
        }

        public void UpdateProduct(Product product)
        {
            using (CBContext db = new CBContext())
            {
                db.Entry(product).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }


        public void DeleteProduct(int ID)
        {
            using (CBContext db = new CBContext())
            {
                var product = db.Products.Find(ID);
            db.Entry(product).State = System.Data.Entity.EntityState.Deleted;
            db.SaveChanges();
                }
        }



    }
}
