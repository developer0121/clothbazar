﻿using ClothBazar.Entities;
using ClotheBazar.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Services
{
    public class ConfigrationService
    {
        public Config Getconfig(string key)
        {
            using (CBContext db = new CBContext())
            {
                return db.Configurations.Find(key);
            }
        }
    }
}
