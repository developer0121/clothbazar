// <auto-generated />
namespace ClotheBazar.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class featured : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(featured));
        
        string IMigrationMetadata.Id
        {
            get { return "202103040645526_featured"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
