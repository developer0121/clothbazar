﻿using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClothBazar.Web.ViewModels
{
    public class HomeViewModel
    {
        public List<Category> Featuredcategories { get; set; }
        public List<Category> Featuredproducts { get; set; }
    }
}