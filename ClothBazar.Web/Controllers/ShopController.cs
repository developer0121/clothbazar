﻿using ClothBazar.Services;
using ClothBazar.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar.Web.Controllers
{
    public class ShopController : Controller
    {
        ProductsService productsService = new ProductsService();

        // GET: Shop
        public ActionResult Checkout()
        {

            CheckoutViewModel model = new CheckoutViewModel();

            var cartProductCookiee = Request.Cookies["CartProduct"];

            if(cartProductCookiee != null)
            {
                model.cartProductIDs = cartProductCookiee.Value.Split('-').Select(x=>int.Parse(x)).ToList();
                model.cartProducts = productsService.GetProducts(model.cartProductIDs);
            }


            return View(model);
        }
    }
}